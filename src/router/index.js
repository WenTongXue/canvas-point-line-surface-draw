import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    redirect: '/canvasDraw'
  },
  {
    path: '/canvasDraw',
    name: 'CanvasDraw',
    component: () => import('../views/CanvasAreaDraw.vue')
  },
  {
    path: '/canvasLineDraw',
    name: 'CanvasLineDraw',
    component: () => import('../views/CanvasLineDraw.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router