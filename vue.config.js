module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '' : '/',
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = 'vue cli 3 - draw'
        return args
      })
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require('postcss-px2rem')({ //配置项，详见官方文档
            remUnit: 100
          }), // 换算的基数
        ]
      }
    }
  },
}