# CanvasPointLineSurfaceDraw

#### 介绍
Canvas点、线、面的操作（点可拖拽、面可移动）
1、可绘制线段，点可进行拖拽。
2、可绘制多边形，点可进行拖拽，区域可进行移动。

#### 项目截图
![Image 项目展示](./img/CanvasDraw.png)

## 开发操作

### 初始化
```
npm install
```

### 建立服务
```
npm run serve
```

### 打包
```
npm run build
```